import { Box, Button, Flex, Icon, Text } from "@chakra-ui/react";

import Image from "next/image";

export default function SelectSignature({ plans, selectStep, planActual, selectPlans }) {

  return (
    <Flex
      w="1000px"
      h="380px"
      flexDirection="column"
      justifyContent="center"
      alignItems="center"
    >
      <Flex
        alignItems="center"
        border="2px solid"
        borderColor="#f9bc00"
        borderRadius={20}
        p="20px"
      >
        <Box w="30%">
          <Image
            src={plans.image}
            alt={plans.label}
            width={200}
            height={200}
          />
        </Box>
        <Flex
          w="100%"
          alignContent="space-evenly"
          textAlign="center"
          flexDirection="column"
          justifyContent="center"
        >
          <Flex alignItems="center">
            <Box w="70%">
              <Text
                textAlign="justify"
                color="blue.400"
                fontWeight="bold"
                fontSize="32px"
              >
                {plans.description}
              </Text>
            </Box>

            <Box w="30%">
              <Text color="#f9bc00" fontWeight="bold" fontSize="40px">
                R$ {plans.price},00
              </Text>
            </Box>
          </Flex>
          <Flex justifyContent="space-between" w="100%" mt={10}>
            {plans.benefits.map((item) => (
              <Flex
                key={item}
                w="100px"
                h="100px"
                alignItems="center"
                justifyContent="center"
                color="blue.400"
              >
                <Icon as={item} w="90px" h="90px" />
              </Flex>
            ))}
          </Flex>
        </Flex>
      </Flex>
      <Button
        h="50px"
        _hover={{ bg: "#f9bc00" }}
        w="50%"
        bottom="-60px"
        fontSize="20px"
        onClick={ () => {
          selectPlans(plans)
          selectStep(planActual + 1)
          }}
      >
        Adquirir
      </Button>
    </Flex>
  );
}
