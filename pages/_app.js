// import "../styles/globals.css";
import '../fonts/bwMitga.css'

import CSSReset from "@chakra-ui/css-reset";
import { ChakraProvider } from "@chakra-ui/react";
import Footer from '../components/footer'
import Head from "next/head";
import Menu from '../components/menu'
import overrides from "../theme";

function MyApp({ Component, pageProps }) {
  return (
    <>
      <Head>
        <title>Care Store by GOW</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <ChakraProvider theme={overrides}>
        <CSSReset />
        <Menu />
        <Component {...pageProps} />
        <Footer />
      </ChakraProvider>
    </>
  );
}

export default MyApp;
