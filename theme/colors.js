const GlobalColors = {
  primary: {
    100: '#90CDF4',
    200: '#63B3ED',
    300: '#4299E1',
    400: '#3182CE',
    500: '#257EB8',
  },
  secondary: {
    100: '#FEFCBF',
    200: '#FAF089',
    300: '#F6E05E',
    400: '#ECC94B',
    500: '#F3C500',
  },

}
export default GlobalColors