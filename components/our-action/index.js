import { Box, Flex, Text } from "@chakra-ui/react";

import Image from "next/image";

export default function OurAction() {
  return (
    <Box w="100%">
      <Text alignSelf="start" fontSize={20}>Nossas Ações</Text>
      <Flex justifyContent="space-between">
        <Box >
          <Image
            src="/care-store-Logo.svg"
            alt="Picture of the author"
            width={200}
            height={200}
          />
        </Box>
        <Box >
          <Image
            src="/doe-amor.png"
            alt="Picture of the author"
            width={150}
            height={150}
          />
        </Box>
        <Box >
          <Image
            src="/produto.png"
            alt="Picture of the author"
            width={150}
            height={150}
          />
        </Box>
      </Flex>
    </Box>
  );
}
