import { Box, Flex, Text, } from "@chakra-ui/react";

import Image from "next/image";

export default function ComoAtuamos() {
  return (
    <Box
      backgroundImage="url(/svg/fundosorrisos_2.svg)"
      backgroundRepeat="round"
    >
      <Text  m="20" fontSize="5xl" textAlign="center">Como Atuamos</Text>
      <Box
        w="60%"
        m="auto"
        display="flex"
        alignItems="center"
        flexDirection="column"
        justifyContent="center"
      >
        <Flex fontSize="xl" m="3" alignItems="center">
          <Image src="/gift.svg" alt="teste" width={150} height={150}/>
          <Text  m="1">Doação e aquisição de produtos e Gifts</Text>
        </Flex>
        <Text textAlign="center" fontSize="xl" m="3">
          
          Os produtos e itens doados, são na verdade
          um poderoso instrumento de mudança.
          São gifts que você pode usar para engajar mais
          pessoas por um mundo melhor.
          Juntos, podemos levar essa mensagem mais
          longe e trazer outras pessoas para mais perto
          do lado solidário da força!
          É a nossa forma de dizer que queremos ver
          mais crianças sorrindo, vovôs e vovós sendo
          assistidos e muitas pessoas que carecem de
          cuidados especiais envolvidas nessa corrente
          do bem.
          Venha com a gente, vista-se com as cores da
          Care Store e mobilize-se pelo movimento
        </Text>
      </Box>

      <Text textAlign="center" fontSize="5xl">Diretrizes da Loja Virtual</Text>
      <Flex
         w="60%"
         m="auto"
         alignItems="center"
         flexDirection="column"
         justifyContent="center"
      >
        <Text textAlign="center" fontSize="xl">
          Os valores dos produtos disponibilizados
          pela Care Store não serão a preço de custo.
          A Loja Social irá acrescentar um valor à
          compra, para que consiga sanar o valor
          gasto com o fornecedor, e esse acréscimo
          será doado às instituições, conforme a
          campanha vigente.
        </Text>
      </Flex>

      <Text textAlign="center" fontSize="5xl">Quem Atendemos?</Text>
      
      <Box
        w="60%"
        m="auto"
        display="flex"
        alignItems="center"
        flexDirection="row"
        justifyContent="center"
      >
        <Text textAlign="center" fontSize="xl">
          Atualmente , contamos com mais de 20 instituições sem fins lucrativos cadastradas, que atuam em varias regiões do Brasil,
          todas seguindo os requisitos de Responsailidade Social e Transparência.
        </Text>
        <Image src="/peoples.svg"  width={200} height={200} />
      </Box>
    </Box>
  );
}
