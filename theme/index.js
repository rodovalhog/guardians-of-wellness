import { theme as baseTheme, extendTheme } from '@chakra-ui/react'

import Breakpoints from './breakpoints'
import ComponentStyles from './components'
import GlobalColors from './colors'
import GlobalStyle from './styles'

const overrides = extendTheme({
  config: {cssVarPrefix: 'guardians-of-wellness'},
  breakpoints: Breakpoints,
  colors: GlobalColors,
  components: ComponentStyles,
  styles: GlobalStyle,
  fonts: {
    heading: 'BwMitga',
    body: 'BwMitga',
  },
  baseTheme,
})

export default overrides
