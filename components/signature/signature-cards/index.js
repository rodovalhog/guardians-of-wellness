import { Box, Button, Flex, ListItem, Text, UnorderedList } from "@chakra-ui/react";

import Image from "next/image";
import { cards } from "./_mock";

export default function SignatureCards({selectPlans, selectStep, planActual}) {
  return (
    <Flex>
      {cards.map((item) => {
        return (
          <Box
            display="flex"
            w="300px"
            h="500px"
            padding="2"
            key={item.id}
            alignItems="center"
            flexDirection="column"
            border="2px solid"
            borderRadius={20}
            borderColor="gray.300"
            mx={4}
            justifyContent="space-evenly"
            position="relative"
            _hover={{
               border: "2px solid", borderColor: "#f9bc00"
              }}
          >
            <Text fontWeight="bold" fontSize="20px" color="#257EB8">
              <strong>{item.label}</strong>
            </Text>
            <Flex flexDirection="column" alignItems="center" justifyContent>
              <Image
                src={item.image}
                alt={item.label}
                width={200}
                height={200}
              />
              <Text fontSize="19px" fontWeight="bold" color="yellow.400">{item.price},00</Text>
              <Text m="2" fontSize="16px" textAlign="center">Ao adquirir essa assinatura você obtem os seguintes beneficios:</Text>
              <Text>
              <UnorderedList>
                 { item.benefits.map( benefit => (<ListItem>{benefit}</ListItem>))}
              </UnorderedList>
              </Text>
            </Flex>
            <Button position="absolute" bottom="-10px" onClick={ () => {
              selectPlans(item)
              selectStep(planActual + 1)
              }}>
              escolher
            </Button>
          </Box>
        );
      })}
    </Flex>
  );
}
