import { Box, Divider, Flex, Text } from "@chakra-ui/react";
import { adesivos, blusas, canecas, chaveiros } from "../mock/_mock-produtos";

import Image from "next/image";

export default function Produtos() {
  return (
    <Box backgroundImage="url(/svg/fundosorrisos_2.svg)" backgroundRepeat="round">
      <Flex w="100%" justifyContent="center" my="40px">
        <Flex w="70%" justifyContent="center" flexDirection="column">
          <Box>
            <Text fontWeight="extrabold" fontSize="30px">
              CAMISETAS
            </Text>
          </Box>

          <Flex justifyContent="space-around" flexWrap="wrap" width="100%">
            {blusas.map((card) => {
              return (
                <Flex
                  my="20px"
                  key={card.id}
                  flexDirection="column"
                  alignItems="center"
                  justifyContent="center"
                  width="25%"
                >
                  <Image
                    src={card.urlImage}
                    alt="Picture of the author"
                    width={200}
                    height={200}
                  />
                  <Text
                    color="#f9bc00"
                    fontWeight="extrabold"
                    fontSize="20px"
                    mt={10}
                  >
                    {card.label}
                  </Text>
                  <Box>{card.component}</Box>
                </Flex>
              );
            })}
          </Flex>
        </Flex>
      </Flex>
      <Flex justifyContent="center">
        <Divider w="80%" />
      </Flex>
      <Flex w="100%" justifyContent="center">
        <Flex w="70%" justifyContent="center" flexDirection="column" my="40px">
          <Box>
            <Text fontWeight="extrabold" fontSize="30px">
              CANECAS
            </Text>
          </Box>

          <Flex justifyContent="space-around" w="100%" flexWrap="wrap">
            {canecas.map((card) => {
              return (
                <Flex
                  w="30%"
                  key={card.id}
                  flexDirection="column"
                  alignItems="center"
                  justifyContent="center"
                >
                  <Image
                    src={card.urlImage}
                    alt="Picture of the author"
                    width={200}
                    height={200}
                  />
                  <Text
                    color="#f9bc00"
                    fontWeight="extrabold"
                    fontSize="20px"
                    mt={10}
                  >
                    {card.label}
                  </Text>
                  <Box>{card.component}</Box>
                </Flex>
              );
            })}
          </Flex>
        </Flex>
      </Flex>
      <Flex justifyContent="center">
        <Divider w="80%" />
      </Flex>

      <Flex w="100%" justifyContent="center" my="40px">
        <Flex w="70%" justifyContent="center" flexDirection="column">
          <Box>
            <Text fontWeight="extrabold" fontSize="30px">
              ADESIVOS
            </Text>
          </Box>

          <Flex justifyContent="space-around" w="100%" flexWrap="wrap">
            {adesivos.map((card) => {
              return (
                <Flex
                  w="25%"
                  my="10px"
                  key={card.id}
                  flexDirection="column"
                  alignItems="center"
                  justifyContent="center"
                >
                  <Image
                    src={card.urlImage}
                    alt="Picture of the author"
                    width={200}
                    height={200}
                  />
                  <Text
                    color="#f9bc00"
                    fontWeight="extrabold"
                    fontSize="20px"
                    mt={10}
                  >
                    {card.label}
                  </Text>
                  <Box>{card.component}</Box>
                </Flex>
              );
            })}
          </Flex>
        </Flex>
      </Flex>

      <Flex justifyContent="center" >
        <Divider w="90%" colorScheme="teal"/>
      </Flex>
      <Flex w="100%" justifyContent="center" my="40px">
        <Flex w="70%" justifyContent="center" flexDirection="column">
          <Box>
            <Text fontWeight="extrabold" fontSize="30px">
              CHAVEIROS
            </Text>
          </Box>

          <Flex justifyContent="space-around" w="100%" flexWrap="wrap">
            {chaveiros.map((card) => {
              return (
                <Flex
                  my="10px"
                  w="33%"
                  key={card.id}
                  flexDirection="column"
                  alignItems="center"
                  justifyContent="center"
                >
                  <Image
                    src={card.urlImage}
                    alt="Picture of the author"
                    width={200}
                    height={200}
                  />
                  <Text
                    color="#f9bc00"
                    fontWeight="extrabold"
                    fontSize="20px"
                    mt={10}
                  >
                    {card.label}
                  </Text>
                  <Box>{card.component}</Box>
                </Flex>
              );
            })}
          </Flex>
        </Flex>
      </Flex>
    </Box>
  );
}
