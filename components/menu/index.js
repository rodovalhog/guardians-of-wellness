import { Box, List, ListItem } from "@chakra-ui/react";

import Link from "next/link";
import Logo from "../logo";
import styles from "../../styles/Menu.module.css";
import { useRouter } from "next/router";

export default function Menu() {
  const router = useRouter();
  const style = (href) => ({
    color: router.pathname === href ? "#F3C500" : "",
  });
  return (
    <Box
      height="50px"
      bg="#257EB8"
      position="sticky"
      top="0"
      zIndex="1"
      display="flex"
      justifyContent="center"
    >
      <Box w="70%">
        <nav className={styles.nav}>
          <Link href="/">
            <a>
              <Logo />
            </a>
          </Link>
          <List display="flex">
            <ListItem>
              <Link href="/">
                <a className={styles.a} style={style("/")}>
                  home_
                </a>
              </Link>
            </ListItem>
            <ListItem>
              <Link href="/quem-somos">
                <a className={styles.a} style={style("/quem-somos")}>
                  quem_somos_
                </a>
              </Link>
            </ListItem>
            <ListItem>
              <Link href="/como-atuamos">
                <a className={styles.a} style={style("/como-atuamos")}>
                  como_atuamos_
                </a>
              </Link>
            </ListItem>
            <ListItem>
              <Link href="/assinatura">
                <a className={styles.a} style={style("/assinatura")}>
                  assinaturas_
                </a>
              </Link>
            </ListItem>
            <ListItem>
              <Link href="/produtos">
                <a className={styles.a} style={style("/produtos")}>
                  produtos_
                </a>
              </Link>
            </ListItem>
          </List>
        </nav>
      </Box>
    </Box>
  );
}
