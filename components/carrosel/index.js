import "react-multi-carousel/lib/styles.css";

import { Box, Flex, Text } from "@chakra-ui/layout";

import Carousel from "react-multi-carousel";
import Image from "next/image";

export const CorouselImage = ({ list }) => {
  const responsive = {
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 3,
      slidesToSlide: 1, // optional, default to 1.
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 2,
      slidesToSlide: 1, // optional, default to 1.
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
      slidesToSlide: 1, // optional, default to 1.
    },
  };
  return (
    <Carousel
      swipeable={false}
      draggable={false}
      // showDots={true}
      responsive={responsive}
      ssr={true}
      infinite={true}
      autoPlay
      autoPlaySpeed={3000}
      keyBoardControl={true}
      customTransition="all .5"
      transitionDuration={500}
      containerClass="carousel-container"
      removeArrowOnDeviceType={["tablet", "mobile"]}
      dotListClass="custom-dot-list-style"
      itemClass="carousel-item-padding-40-px"
    >
      {list.map((card) => {
        return (
          <Flex
            key={card.id}
            flexDirection="column"
            alignItems="center"
            justifyContent="center"
          >
            <Image
              src={card.urlImage}
              alt="Picture of the author"
              width={card.width || 200}
              height={card.height || 200}
            />
            <Text
              color="#f9bc00"
              fontWeight="extrabold"
              fontSize="20px"
              mt={10}
            >
              {card.label}
            </Text>
            <Box>
              {card.component}
            </Box>
          </Flex>
        );
      })}
    </Carousel>
  );
};
