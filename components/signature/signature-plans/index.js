import { Box, Button, Flex, Icon, Text, Tooltip } from "@chakra-ui/react";

import Image from "next/image";
import LayoutPaymentCard from "../../layout-payment-card";
import { LayoutPix } from "../../layout-pix";
import { useState } from "react";

export default function SignaturePlans({ plans }) {
  const [methodPayment, setMethodPayment] = useState("cardCredit");

  const handlePayment = () => {
    switch (methodPayment) {
      case "cardCredit":
        return <LayoutPaymentCard payment="Credito" bg="#257EB8" />;
      case "cardDebit":
        return <LayoutPaymentCard payment="Debito" bg="#f6e05e"/>;
      case "pix":
        return <LayoutPix />;
    }
  };
  return (
    <Box>
    <Flex>
      <Flex
        bg="yellow.300"
        flexDirection="column"
        border="1px solid"
        borderRadius="25px"
        h="380px"
        w="250px"
      >
        <Box p={5} display="flex" justifyContent="center">
          <Image src={plans.image} alt={plans.label} width={100} height={100} />
        </Box>
        <Box p="2">
          <Text fontWeight="extrabold" textAlign="center">Resumo:</Text>
          <Text textAlign="center" >{plans.description}</Text>
          <Text textAlign="center">{plans.benefitsInfo}</Text>

          <Text
            textAlign="center"
            fontSize="28px"
            fontWeight="extrabold"
            color="#fff"
          >
            R$ {plans.price},00
          </Text>
        </Box>
      </Flex>
      <Flex flexDirection="column">
        <Flex justifyContent="space-evenly">
          <Box p={2} m={2}>
            <Button
              _hover={{ bg: "#f9bc00" }}
              onClick={() => setMethodPayment("cardCredit")}
            >
              Cartão de credito
            </Button>
          </Box>
          <Box p={2} m={2}>
            <Button
              _hover={{ bg: "#f9bc00" }}
              onClick={() => setMethodPayment("cardDebit")}
            >
              Cartão de Debito
            </Button>
          </Box>
          <Box p={2} m={2}>
            <Button
              _hover={{ bg: "#f9bc00" }}
              onClick={() => setMethodPayment("pix")}
            >
              PIX
            </Button>
          </Box>
        </Flex>

        <Box>
          {handlePayment()}
          {/* <LayoutPix /> */}
          {/* <LayoutPaymentCard  payment="Credito visa"/> */}
        </Box>
      </Flex>
    </Flex>
    <Flex justifyContent="center">

    <Button
        h="50px"
        _hover={{ bg: "#f9bc00" }}
        w="50%"
        bottom="-60px"
        fontSize="20px"

      >
        Pagar
      </Button>
    </Flex>
    </Box>
  );
}
