import { Button, Flex, Link, Text } from "@chakra-ui/react";

export default function HighlightedText() {
  return (
    <Flex alignItems="center" flexDirection="column">
    <Text fontSize={32} fontWeight="bold" m="2" textAlign="center">
      Como posso contribuir?
    </Text>
    <Text fontSize={32} color="blue.600" textAlign="center">
      Por meio da nossa assinatura de doações mensais você pode fazer a diferença de forma simples e descomplicada.
      E o que você ganha por ser um doador mensal? produtos mensais exclusivos na sua casa.
    </Text>
    <Button borderRadius={50} w="250px" h="80px" m="5">
            <Link href="/assinatura">
              <a style={{ fontSize: "20px" }}>Seja um Guardião</a>
            </Link>
          </Button>
    </Flex>
  );
}
