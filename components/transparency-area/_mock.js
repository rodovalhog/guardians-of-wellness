import { BsGift, BsPeople } from "react-icons/bs";

import { FaPeopleCarry } from "react-icons/fa";
import { GrMoney } from "react-icons/gr";

export const  cards = [
  { value: 58647, label: "arrecadado", image: GrMoney },
  { value: 222, label: "doadores mensais", image: BsPeople },
  { value: 4567, label: "voluntarios", image: FaPeopleCarry },
  { value: 999, label: "produtos vendidos", image: BsGift },

];
