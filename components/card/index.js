import { Box, Icon, Text } from "@chakra-ui/react";

import CountUp from "react-countup";

export default function Card({ value, label, image }) {

  return (
    <Box
      display="flex"
      flexDirection="column"
      alignItems="center"
      textAlign="center"
      w={200}
      mx={10}
      border="1px solid"
      borderRadius={20}
      borderColor="gray.300"
    >
      <Box m={4}>
      <Icon as={image} width={100} height={100} />
      <Text fontSize={40} fontWeight="bold">
        <CountUp end={value} start={0} duration={10} />
      </Text>
      <Text fontSize={25} fontWeight="bold" color="#257EB8" textAlign="center">
        {label}
      </Text>
      </Box>
    </Box>
  );
}
