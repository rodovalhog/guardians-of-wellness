import { Box, Flex, Input, InputGroup, Text } from "@chakra-ui/react";

import Image from "next/image";

export function LayoutPix() {
  return (
    <Flex
      justifyContent="center"
      alignItems="center"
      flexDirection="column"
      w="500px"
      h="300px"
      mx="5"
      p="20px"
    >
      <Box>
        <Image src="/qrcode-pix.png" alt="pix" width={200} height={200} />
      </Box>
      <Flex flexDirection="column" alignItems="center">
        <InputGroup>
          <Input type="tel" placeholder="50,00" />
        </InputGroup>
        <Box mt="10px">
          <Text>Guardians of Wellness</Text>
        </Box>
      </Flex>
    </Flex>
  );
}
