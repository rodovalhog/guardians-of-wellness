import { Button } from "@chakra-ui/react";
import Link from "next/link";
import React from "react";

export const canecas = [
  {
    id: "1",
    urlImage: "/produtos/canecas/1.png",
    label: "R$ 25,00",
    component: (
      <Button borderRadius={50} mt="15px" >
        <Link href="/produtos">
          <a>compre</a>
        </Link>
      </Button>
    ),
  },
  {
    id: "3",
    urlImage: "/produtos/canecas/2.png",
    label: "R$ 25,00",
    component: (
      <Button borderRadius={50} mt="15px" >
        <Link href="/produtos">
          <a>compre</a>
        </Link>
      </Button>
    ),
  },
  {
    id: "2",
    urlImage: "/produtos/canecas/3.png",
    label: "R$ 25,00",
    component: (
      <Button borderRadius={50} mt="15px" >
        <Link href="/produtos">
          <a>compre</a>
        </Link>
      </Button>
    ),
  },
  {
    id: "4",
    urlImage: "/produtos/canecas/4.png",
    label: "R$ 25,00",
    component: (
      <Button borderRadius={50} mt="15px" >
        <Link href="/produtos">
          <a>compre</a>
        </Link>
      </Button>
    ),
  },
  {
    id: "5",
    urlImage: "/produtos/canecas/5.png",
    label: "R$ 25,00",
    component: (
      <Button borderRadius={50} mt="15px" >
        <Link href="/produtos">
          <a>compre</a>
        </Link>
      </Button>
    ),
  },
];

export const chaveiros = [
  {
    id: "1",
    urlImage: "/produtos/chaveiros/1.png",
    label: "R$ 25,00",
    component: (
      <Button borderRadius={50} mt="15px" >
        <Link href="/produtos">
          <a>compre</a>
        </Link>
      </Button>
    ),
  },
  {
    id: "3",
    urlImage: "/produtos/chaveiros/2.png",
    label: "R$ 25,00",
    component: (
      <Button borderRadius={50} mt="15px" >
        <Link href="/produtos">
          <a>compre</a>
        </Link>
      </Button>
    ),
  },
  {
    id: "2",
    urlImage: "/produtos/chaveiros/3.png",
    label: "R$ 25,00",
    component: (
      <Button borderRadius={50} mt="15px" >
        <Link href="/produtos">
          <a>compre</a>
        </Link>
      </Button>
    ),
  },
  {
    id: "4",
    urlImage: "/produtos/chaveiros/4.png",
    label: "R$ 25,00",
    component: (
      <Button borderRadius={50} mt="15px" >
        <Link href="/produtos">
          <a>compre</a>
        </Link>
      </Button>
    ),
  },
  {
    id: "5",
    urlImage: "/produtos/chaveiros/5.png",
    label: "R$ 25,00",
    component: (
      <Button borderRadius={50} mt="15px" >
        <Link href="/produtos">
          <a>compre</a>
        </Link>
      </Button>
    ),
  },
];

export const adesivos = [
  {
    id: "1",
    urlImage: "/produtos/adesivos/1.png",
    label: "R$ 25,00",
    component: (
      <Button borderRadius={50} mt="15px" >
        <Link href="/produtos">
          <a>compre</a>
        </Link>
      </Button>
    ),
  },
  {
    id: "3",
    urlImage: "/produtos/adesivos/2.png",
    label: "R$ 25,00",
    component: (
      <Button borderRadius={50} mt="15px" >
        <Link href="/produtos">
          <a>compre</a>
        </Link>
      </Button>
    ),
  },
  {
    id: "2",
    urlImage: "/produtos/adesivos/3.png",
    label: "R$ 25,00",
    component: (
      <Button borderRadius={50} mt="15px" >
        <Link href="/produtos">
          <a>compre</a>
        </Link>
      </Button>
    ),
  },
  {
    id: "4",
    urlImage: "/produtos/adesivos/4.png",
    label: "R$ 25,00",
    component: (
      <Button borderRadius={50} mt="15px" >
        <Link href="/produtos">
          <a>compre</a>
        </Link>
      </Button>
    ),
  },
  {
    id: "5",
    urlImage: "/produtos/adesivos/5.png",
    label: "R$ 25,00",
    component: (
      <Button borderRadius={50} mt="15px" >
        <Link href="/produtos">
          <a>compre</a>
        </Link>
      </Button>
    ),
  },
  {
    id: "6",
    urlImage: "/produtos/adesivos/6.png",
    label: "R$ 25,00",
    component: (
      <Button borderRadius={50} mt="15px" >
        <Link href="/produtos">
          <a>compre</a>
        </Link>
      </Button>
    ),
  },
  {
    id: "7",
    urlImage: "/produtos/adesivos/7.png",
    label: "R$ 25,00",
    component: (
      <Button borderRadius={50} mt="15px" >
        <Link href="/produtos">
          <a>compre</a>
        </Link>
      </Button>
    ),
  },
];

export const blusas = [
  {
    id: "1",
    urlImage: "/produtos/blusas/1.png",
    label: "R$ 50,00",
    component: (
      <Button borderRadius={50} mt="15px" >
        <Link href="/produtos">
          <a>compre</a>
        </Link>
      </Button>
    ),
  },
  {
    id: "3",
    urlImage: "/produtos/blusas/2.png",
    label: "R$ 50,00",
    component: (
      <Button borderRadius={50} mt="15px" >
        <Link href="/produtos">
          <a>compre</a>
        </Link>
      </Button>
    ),
  },
  {
    id: "2",
    urlImage: "/produtos/blusas/3.png",
    label: "R$ 50,00",
    component: (
      <Button borderRadius={50} mt="15px" >
        <Link href="/produtos">
          <a>compre</a>
        </Link>
      </Button>
    ),
  },
  {
    id: "4",
    urlImage: "/produtos/blusas/4.png",
    label: "R$ 50,00",
    component: (
      <Button borderRadius={50} mt="15px" >
        <Link href="/produtos">
          <a>compre</a>
        </Link>
      </Button>
    ),
  },
  {
    id: "5",
    urlImage: "/produtos/blusas/5.png",
    label: "R$ 50,00",
    component: (
      <Button borderRadius={50} mt="15px" >
        <Link href="/produtos">
          <a>compre</a>
        </Link>
      </Button>
    ),
  },
  {
    id: "6",
    urlImage: "/produtos/blusas/6.png",
    label: "R$ 50,00",
    component: (
      <Button borderRadius={50} mt="15px" >
        <Link href="/produtos">
          <a>compre</a>
        </Link>
      </Button>
    ),
  },
  {
    id: "7",
    urlImage: "/produtos/blusas/7.png",
    label: "R$ 50,00",
    component: (
      <Button borderRadius={50} mt="15px" >
        <Link href="/produtos">
          <a>compre</a>
        </Link>
      </Button>
    ),
  },
  {
    id: "8",
    urlImage: "/produtos/blusas/8.png",
    label: "R$ 50,00",
    component: (
      <Button borderRadius={50} mt="15px" >
        <Link href="/produtos">
          <a>compre</a>
        </Link>
      </Button>
    ),
  },
  {
    id: "9",
    urlImage: "/produtos/blusas/9.png",
    label: "R$ 50,00",
    component: (
      <Button borderRadius={50} mt="15px" >
        <Link href="/produtos">
          <a>compre</a>
        </Link>
      </Button>
    ),
  },
  {
    id: "10",
    urlImage: "/produtos/blusas/10.png",
    label: "R$ 50,00",
    component: (
      <Button borderRadius={50} mt="15px" >
        <Link href="/produtos">
          <a>compre</a>
        </Link>
      </Button>
    ),
  },
  {
    id: "11",
    urlImage: "/produtos/blusas/11.png",
    label: "R$ 50,00",
    component: (
      <Button borderRadius={50} mt="15px" >
        <Link href="/produtos">
          <a>compre</a>
        </Link>
      </Button>
    ),
  },
  {
    id: "12",
    urlImage: "/produtos/blusas/12.png",
    label: "R$ 50,00",
    component: (
      <Button borderRadius={50} mt="15px" >
        <Link href="/produtos">
          <a>compre</a>
        </Link>
      </Button>
    ),
  },
];

export const planos = [
  {
    id: 1,
    label: "R$ 50,00",
    urlImage: "/mascote/felicio-3.png",
    component: (
      <Button borderRadius={50} mt="15px" >
        <Link href="/produtos">
          <a>compre</a>
        </Link>
      </Button>
    ),
  },
  {
    id: 3,
    label: "R$ 25,00",
    urlImage: "/mascote/felicio-1.png",
    component: (
      <Button borderRadius={50} mt="15px" >
        <Link href="/produtos">
          <a>compre</a>
        </Link>
      </Button>
    ),
  },
  {
    id: 2,
    label: "R$ 100,00",
    urlImage: "/mascote/felicio-6.png",
    component: (
      <Button borderRadius={50} mt="15px" >
        <Link href="/produtos">
          <a>compre</a>
        </Link>
      </Button>
    ),
  },
];
