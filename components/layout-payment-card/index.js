import {
  Box,
  Button,
  Editable,
  EditableInput,
  EditablePreview,
  Flex,
  FormLabel,
  HStack,
  Icon,
  Input,
  InputGroup,
  InputLeftAddon,
  PinInput,
  PinInputField,
  Text,
  Tooltip,
} from "@chakra-ui/react";
import {MdCheck, MdClose} from 'react-icons/md'
import { useEffect, useState } from "react";

import Image from "next/image";
import { InputEditable } from "../input-editable";

// GiCheckMark



export default function LayoutPaymentCard({ name, cardNumber, payment, bg }) {
  const [value, setValue] = useState("NUMERO DO CARTÃO");
  const [numCard, setNumCard] = useState('')
  function checkLuhn(value) {
    // remove all non digit characters
    var value = value.replaceAll('-', '')
     value = value.replace(/\D/g, "");
    var sum = 0;
    var shouldDouble = false;
    // loop through values starting at the rightmost side
    for (var i = value.length - 1; i >= 0; i--) {
      var digit = parseInt(value.charAt(i));

      if (shouldDouble) {
        if ((digit *= 2) > 9) digit -= 9;
      }

      sum += digit;
      shouldDouble = !shouldDouble;
    }
    return sum % 10 == 0;
  }

  function mcc(v) {
    setNumCard(v)
    v = v.replace(/\D/g, ""); // Permite apenas dígitos
    v = v.replace(/(\d{4})/g, "$1-"); // Coloca um ponto a cada 4 caracteres
    v = v.replace(/\.$/, ""); // Remove o ponto se estiver sobrando
    v = v.substring(0, 19); // Limita o tamanho
    v.s;
    setValue(v);
  }

  useEffect(() => {}, []);
  return (
    <Flex>
      <Box
        bg={bg}
        border="1px solid"
        alignContent="center"
        borderRadius="20px"
        p="20px"
        w="500px"
        h="300px"
        mx="5"
      >
        <Box>
          <Text color="white" fontWeight="extrabold">{payment}</Text>
        </Box>
        <Flex alignItems="center">
       
        <Box w="250px">
          <Editable
          fontWeight="extrabold"
            bg="white"
            my="10px"
          
            type="number"
            p={2}
            borderRadius="10px"
            defaultValue="NUMERO DO CARTÃO"
            border="1px solid"
            value={value}
            onChange={(event) => {
              mcc(event);
            }}
          >
            <EditablePreview />
            <EditableInput />
          </Editable>
        </Box>
        {numCard.length >= 16 && <Icon as={checkLuhn(value) ? MdCheck : MdClose} color={checkLuhn(value) ? "#10ff18" : "#f30000"} w="50px" h="50px"/>}

</Flex>
        <Editable
          type="number"
          bg="white"
          p={2}
          borderRadius="10px"
          defaultValue="NOME DO CARTÃO"
          border="1px solid"
          fontWeight="extrabold"
        >
          <EditablePreview />
          <EditableInput />
        </Editable>
        <Flex alignItems="flex-end">
          <Box mr={2}>
            <FormLabel>VENCIMENTO</FormLabel>
            <Input type="date" bg="white" />
          </Box>

          <Box w="100px">
            <FormLabel>CVV</FormLabel>
            <Input bg="white" />
          </Box>
        </Flex>
        <Flex justifyContent="flex-end">


     
        </Flex>
      </Box>
    </Flex>
  );
}
