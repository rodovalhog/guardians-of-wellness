import { Box, Button, Flex, Heading } from "@chakra-ui/react";

import Image from "next/image";
import Link from "next/link";

export default function Hero() {
  return (
    <Flex alignItems="center" justifyContent="space-evenly">
      <Box display="flex" flexDirection="column">
        <Flex
          mb={10}
          justifyContent="center"
          alignItems="center"
          flexDirection="column"
        >
          <Heading as="h1" size="2xl" color="#f9bc00" m={2}>
            SEJA A
          </Heading>
          <Heading as="h1" size="4xl" color="#257EB8" m={2}>
            DIFERENÇA
          </Heading>
          <Heading as="h1" size="2xl" color="#f9bc00" m={2}>
            NO MUNDO
          </Heading>
        </Flex>
        <Box display="flex" justifyContent="center">
          <Button borderRadius={50} w="200px" h="80px">
            <Link href="/quem-somos">
              <a style={{fontSize: "20px"}}>Saiba Mais</a>
            </Link>
          </Button>
        </Box>
      </Box>

      <Image
        src="/mascote/felicio-7.png"
        alt="Picture of the author"
        width={350}
        height={350}
      />
    </Flex>
  );
}
