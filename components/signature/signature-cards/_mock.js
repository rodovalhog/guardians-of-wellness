BiJoystick

import {BiBeer, BiBot, BiCamera, BiJoystick} from 'react-icons/bi'

export const cards = [
  {
    id: 1,
    price: 15,
    label: "Plano 1",
    image: "/mascote/felicio-3.png",
    description:
      "Ao adquirir essa assinatura você obtem os seguintes beneficios:",
    benefits: ['Adesivos', 'Frete Gratis'],
    benefitsInfo: "Brinde exclusivo, sorteios, participação de ventos",

  },
  {
    id: 2,
    price: 30,
    label: "Plano 2",
    image: "/mascote/felicio-1.png",
    description:
    "Ao adquirir essa assinatura você obtem os seguintes beneficios:",
    benefits: ['Chaveiro', 'Frete Gratis'],
    benefitsInfo: "Brinde exclusivo, sorteios, participação de ventos",

  },
  {
    id: 3,
    price: 50,
    label: "Plano 3",
    image: "/mascote/felicio-6.png",
    description:
    "Ao adquirir essa assinatura você obtem os seguintes beneficios:",
    benefitsInfo: "Brinde exclusivo, sorteios, participação de ventos",
    benefits: ['Caneca', 'Frete Gratis'],
  },
  {
    id: 4,
    price: 100,
    label: "Plano 4",
    image: "/mascote/felicio-7.png",
    description:
    "Ao adquirir essa assinatura você obtem os seguintes beneficios:",
    benefits: ['Camiseta', 'Frete Gratis'],
    benefitsInfo: "Brinde exclusivo, sorteios, participação de ventos",

  },
];
