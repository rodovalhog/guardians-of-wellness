import { Box, Center, Container, Flex, Text } from "@chakra-ui/react";

import { CorouselImage as Carousel } from "../components/carrosel";
import Image from "next/image";
import {TimeLine} from '../components/time-line'

export default function QuemSomos() {
  const list = [
    {
      id: "1",
      urlImage: "/foto_1.jpeg",
      label: "Ação de Natal",
      width: 320,
      height: 250
    },
    {
      id: "3",
      urlImage: "/foto_2.jpeg",
      label: "Ação de Páscoa",
      width: 320,
      height: 250
    },
    {
      id: "2",
      urlImage: "/foto_3.jpeg",
      label: "Ação de Páscoa",
      width: 320,
      height: 250
    },
    {
      id: "4",
      urlImage: "/foto_4.jpeg",
      label: "Ação de dia das crianças",
      width: 320,
      height: 250
    },
    {
      id: "5",
      urlImage: "/foto_5.jpeg",
      label: "Ação de Carnaval Solidário",
      width: 320,
      height: 250
    },
  ];

  return (
    <Box
      backgroundImage="url(/svg/fundosorrisos_2.svg)"
      backgroundRepeat="round"
    >
      <Text fontSize="5xl" textAlign="center" m="20">Quem Somos</Text>
      <Flex w="100%" justifyContent="center" h="400px">
        <Box w="70%">
          <Carousel  list={list} />
        </Box>
      </Flex>

      <Box
        w="100%"
        bg="#f6e05e"
        display="flex"
        justifyContent="center"
        alignItems="center"
      >
        <Text
          fontSize={20}
          color="blue.600"
          margin="7"
          textAlign="center"
          w="70%"
        >
          A Invillia Social busca fazer a diferença na sociedade como um todo, em aspectos sociais, ambientais e econômicos. Acreditamos e valorizamos as pessoas, os sonhos, a simplicidade, o trabalho, com-
          promisso, disciplina e em ouvir. Por isso, valorizamos e promovemos ações sociais, em parceria com os colaboradores, com objetivo de fazer sorrisos e alcançar pessoas de diferentes formas.
          Dedicar-se as nossas crenças e valores significa construir uma história para que todo o nosso trabalho, tenha um propósito, um sentido e um significado maior do que apenas a simples execução de tarefas.
          Os projetos sociais, nasceram de um desejo de mudar a realidade. Esse desejo é de fazer sorrisos. De forma geral, esses projetos transformam a realidade social e são construídos a partir do diagnóstico 
          de uma problemática com base em planejamento, estruturação, objetivos, resultados e atividades.
          Assim é a Care Store, uma entidade Invillia Social.
        </Text>
      </Box>

      <Flex w="100%" justifyContent="center" >
        <Box w="80%">
          <Text m="7" fontSize={32}>Nossa Historia</Text>
          <TimeLine />
        </Box>
      </Flex>
      
    </Box>
  );
}
