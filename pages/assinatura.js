import { Box, Container, Flex, Icon, Text } from "@chakra-ui/react";

import { ImArrowLeft2 } from "react-icons/im";
import SelectSignature from "../components/signature/select-signature";
import SignatureCards from "../components/signature/signature-cards";
import SignaturePlans from "../components/signature/signature-plans";
import { useState } from "react";

export default function Assinatura() {
  const [step, setStep] = useState(1);
  const [selectPlans, setSelectPlans] = useState(null);

  const stepSignature = () => {
    switch (step) {
      case 1:
        return (
          <SignatureCards
            selectPlans={setSelectPlans}
            selectStep={setStep}
            planActual={step}
          />
        );
      case 2:
        return (
          <SignaturePlans plans={selectPlans} />
        );
        
    }
  };

  return (
    <Flex h="90vh" w="100%" flexDirection="column" alignItems="center" backgroundImage="url(/svg/fundosorrisos_2.svg)" backgroundRepeat="round">
      <Box my="80px" w="80%" alignItems="center" justifyContent="center">
        <Text color="#f9bc00" textAlign="center" fontWeight="extrabold" fontSize="40px">ESCOLHA SEU PLANO</Text>
      </Box>
      <Flex justifyContent="center"  alignItems="center" w="80%">
        <Box>
          {step > 1 && (
            <Box>
              <Icon
                color="#f9bc00"
                as={ImArrowLeft2}
                w={10}
                h={10}
                onClick={() => {
                  setStep(step - 1);
                }}
                cursor="pointer"
              />
            </Box>
          )}
          <Box h="400px">{stepSignature()}</Box>
        </Box>
      </Flex>
    </Flex>
  );
}
