import { Box } from "@chakra-ui/react";
import Card from "../card";
import Image from "next/image";
import { cards } from "./_mock";

export default function TransparencyArea() {
  return (
    <Box display="flex" my={20} justifyContent="space-between">
      {cards.map((card) => {
         return <Card key={card.label} value={card.value} label={card.label} image={card.image} />
      })}
    </Box>
  );
}
