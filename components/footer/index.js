import { Box, Flex, Grid, Icon, List, ListItem, Text } from "@chakra-ui/react";
import { SiFacebook, SiInstagram, SiLinkedin, SiYoutube } from "react-icons/si";

import Image from "next/image";
import Link from "next/link";
import styles from "../../styles/Footer.module.css";

export default function Footer() {
  return (
    <Box
      bg="#257EB8"
      display="flex"
      justifyContent="center"
      alignItems="center"
      w="100%"
      position="absolute"
    >
      <Box w="70%" display="flex" justifyContent="space-between">
        <Box display="flex" alignItems="center">
          <Image
            src="/mascote/felicio-1.png"
            alt="Picture of the author"
            width={80}
            height={80}
          />
        </Box>

        <Box>
          <nav>
            <List>
              <ListItem>
                <Link href="/">
                  <a className={styles.a}>Home</a>
                </Link>
              </ListItem>
              <ListItem>
                <Link href="/como-atuamos">
                  <a className={styles.a}>Como atuamos</a>
                </Link>
              </ListItem>
              <ListItem>
                <Link href="/assinatura">
                  <a className={styles.a}>Assinaturas</a>
                </Link>
              </ListItem>
              <ListItem>
                <Link href="/produtos">
                  <a className={styles.a}>Produtos</a>
                </Link>
              </ListItem>
            </List>
          </nav>
        </Box>
        <Flex
          flexDirection="column"
          alignItems="center"
          justifyContent="center"
        >
          <Text color="white">EMPRESASAS PARCEIRAS</Text>
          <Image
            src="/invillia.png"
            alt="Picture of the author"
            width={100}
            height={30}
          />
        </Flex>
        <Flex>
          <Flex
            flexDirection="column"
            justifyContent="center"
            alignItems="center"
          >
            <Box>
              <Text color="white">Encontre-nos</Text>
            </Box>
            <Box>
              <Icon color="white" p={1} as={SiFacebook} w={7} h={7} />
              <Icon color="white" p={1} as={SiInstagram} w={7} h={7} />
              <Icon color="white" p={1} as={SiLinkedin} w={7} h={7} />
              <Icon color="white" p={1} as={SiYoutube} w={7} h={7} />
            </Box>
          </Flex>
        </Flex>
      </Box>
    </Box>
  );
}
