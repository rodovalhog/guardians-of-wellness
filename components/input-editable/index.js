import { Editable, EditableInput, EditablePreview } from "@chakra-ui/react"

export function InputEditable (value) {
  return (
<Editable defaultValue={value.slice()}>
  <EditablePreview />
  <EditableInput />
</Editable>
  )
}