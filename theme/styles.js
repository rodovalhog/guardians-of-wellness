const styles =  {
  global: {
    body: {
      maxWidth: '2000px',
      minWidth: '320px',

      fontFamily: 'body',
      color: 'neutral.400',
      bg: 'white',
      transitionProperty: 'background-color',
      transitionDuration: 'normal',
      lineHeight: 'base',
    },
    '*::placeholder': {
      color: 'neutral.200',
    },
    '*, *::before, &::after': {
      borderColor: 'neutral.200',
      wordWrap: 'break-word',
    },
    h1: {
      fontSize: '24px',
    },
    h2: {
      fontSize: '16px',
    },
    h3: {
      fontSize: '14px',
    },
    h4: {
      fontSize: '12px',
    },
    h5: {
      fontSize: '10px',
    },
    h6: {
      fontSize: '8px',
    },
  },
}

export default styles