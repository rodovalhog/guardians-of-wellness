import {
  Box,
  Flex,
  Slider,
  SliderFilledTrack,
  SliderThumb,
  SliderTrack,
  Text,
} from "@chakra-ui/react";

import { useState } from "react";

export function TimeLine() {
  const [date, setDate] = useState(0);
  console.log("~ date", date)
  const historyTimeLine = () => {
    if (date < 14) {
      return (
        <Flex
          justifyContent="center"
          alignItems="center"
          flexDirection="column"
        >
          <Text fontSize="30px" fontWeight="extrabold">
            2014
          </Text>
          <Text mt="20px" fontSize="18px">
            6.091 SORRISOS, incluindo crianças e idosos em mais de 25
            instituições
          </Text>
        </Flex>
      );
    }

    if (date >= 15 && date <= 28) {
      return (
        <Flex
          justifyContent="center"
          alignItems="center"
          flexDirection="column"
        >
          <Text fontSize="30px" fontWeight="extrabold">
            2015
          </Text>
          <Text mt="20px" fontSize="18px">
            Também houve muito engajamento e atingimos 9.791 SORRISOS.
          </Text>
        </Flex>
      );
    }

    if (date >= 28 && date <= 42) {
      return (
        <Flex
          justifyContent="center"
          alignItems="center"
          flexDirection="column"
        >
          <Text fontSize="30px" fontWeight="extrabold">
            2016
          </Text>
          <Text mt="20px" fontSize="18px">
            Revisitamos o Plano de Doações, onde setorizamos as doações para
            evitar doar o mesmo item (cobertores, por exemplo) para a mesma
            criança. Ainda, ampliamos para moradores de rua e aderi- mos a
            campanha de doação de móveis para crianças (berços, colchões e
            afins). Neste mesmo ano fizemos 3.200 sorrisos.
          </Text>
        </Flex>
      );
    }
    if (date >= 42 && date <= 56) {
      return (
        <Flex
          justifyContent="center"
          alignItems="center"
          flexDirection="column"
        >
          <Text fontSize="30px" fontWeight="extrabold">
            2017
          </Text>
          <Text mt="20px" fontSize="18px">
            A pedido das instituições passa- mos a doar Cestas Básicas e itens
            de higiene que substituíram brinquedos, por exemplo. Aproximadamente
            2.600 sorri- sos (as cestas básicas e móveis não contam sorrisos).
          </Text>
        </Flex>
      );
    }
    if (date >= 56 && date <= 69) {
      return (
        <Flex
          justifyContent="center"
          alignItems="center"
          flexDirection="column"
        >
          <Text fontSize="30px" fontWeight="extrabold">
            2018
          </Text>
          <Text mt="20px" fontSize="18px">
            Fizemos a rebrand da Care Store e lançamos campanhas de ampliação de
            campanhas internas (incluindo a Copa do Mundo e Black Friday),
            atingimos nossa meta e chegamos a 2.800 sorrisos.
          </Text>
        </Flex>
      );
    }
    if (date >= 70 && date <= 84) {
      return (
        <Flex
          justifyContent="center"
          alignItems="center"
          flexDirection="column"
        >
          <Text fontSize="30px" fontWeight="extrabold">
            2019
          </Text>
          <Text mt="20px" fontSize="18px">
            Iniciamos em modo Beta a Care School com inglês para crianças
            carentes e a loja social ganhou o atendimento via Moderninha do
            PagSeguro. Quanto aos sorrisos chegamos a 3.200 sorrisos visto que
            aumentamos o Orçamento Social com o Outlet Solidário na venda de
            equi- pamentos de TI a serem substituídos, mas em bom estado.
          </Text>
        </Flex>
      );
    }

    if (date >= 84 && date <= 100) {
      return (
        <Flex
          justifyContent="center"
          alignItems="center"
          flexDirection="column"
        >
          <Text fontSize="30px" fontWeight="extrabold">
            2021
          </Text>
          <Text mt="20px" fontSize="18px">
            Vamos construir juntos?
          </Text>
        </Flex>
      );
    }
  };
  return (
    <Box h="400px">
      <Flex justifyContent="space-between">
        <Box>
          <Text m={0} p={0} fontWeight="extrabold" fontSize="15px" w="16.6%">
            2014
          </Text>
        </Box>
        <Box>
          <Text m={0} p={0} fontWeight="extrabold" fontSize="15px" w="16.6%">
            2015
          </Text>
        </Box>
        <Box>
          <Text m={0} p={0} fontWeight="extrabold" fontSize="15px" w="16.6%">
            2016
          </Text>
        </Box>
        <Box>
          <Text m={0} p={0} fontWeight="extrabold" fontSize="15px" w="16.6%">
            2017
          </Text>
        </Box>
        <Box>
          <Text m={0} p={0} fontWeight="extrabold" fontSize="15px" w="16.6%">
            2018
          </Text>
        </Box>
        <Box>
          <Text m={0} p={0} fontWeight="extrabold" fontSize="15px" w="16.6%">
            2019
          </Text>
        </Box>
        <Box>
          <Text m={0} p={0} fontWeight="extrabold" fontSize="15px" w="16.6%">
            2021
          </Text>
        </Box>
      </Flex>
      <Slider
        mt="40px"
        aria-label="slider-ex-1"
        defaultValue={date}
        onChange={(value) => setDate(value)}
      >
        <SliderTrack>
          <SliderFilledTrack />
        </SliderTrack>
        <SliderThumb bg="#f9bc00" w="40px" h="40px" />
      </Slider>
      <Flex justifyContent="center" mt="30px">
        <Box>{historyTimeLine()}</Box>
      </Flex>

      <Flex></Flex>
    </Box>
  );
}
