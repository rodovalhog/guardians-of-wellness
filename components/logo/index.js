import { Flex, Text } from "@chakra-ui/react";

import Image from "next/image";

export default function Logo() {
  return (
    <Flex  alignItems="center" flexDirection="column" >
      <Image
        src="/mascote/felicio-3.png"
        alt="Picture of the author"
        width={30}
        height={30}
      />
      <Text color="#fff">care_store</Text>
    </Flex>
  );
}
