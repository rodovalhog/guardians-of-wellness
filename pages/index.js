import { Box, Divider, Flex, Text } from "@chakra-ui/react";
import {
  adesivos,
  blusas,
  canecas,
  chaveiros,
  planos,
} from "../mock/_mock-produtos";

import { CorouselImage as Carousel } from "../components/carrosel";
import Hero from "../components/hero";
import HighlightedText from "../components/highlighted-text";
import Image from "next/image";
// import OurAction from "../components/our-action";
import TransparencyArea from "../components/transparency-area";

export default function Home() {
  const newCanecas = canecas.slice(0, 4);
  const newBlusas = blusas.slice(0, 3);
  const newAdesivos = adesivos.slice(0, 3);
  const newChaveiro = chaveiros.slice(0, 3);

  const listProdutos = [
    ...newCanecas,
    ...newBlusas,
    ...newAdesivos,
    ...newChaveiro,
    ...planos,
  ];

  return (
    <Box
      display="flex"
      alignItems="center"
      flexDirection="column"
      justifyContent="center"
      h="80%"
    >
      <Flex w="100%" justifyContent="center" alignItems="center" h="400px">
        <Box w="70%">
          <Hero />
        </Box>
      </Flex>
      <Box
        h="400px"
        w="100%"
        display="flex"
        justifyContent="center"
        alignItems="center"
        bg="yellow.300"
      >
        <Box w="70%">
          <HighlightedText />
        </Box>
      </Box>
      <Text textAlign="center" color="#3182ce" fontWeight="extrabold" fontSize="30px" mt="20px" >
        CONFIRA NOSSO PRODUTO
      </Text>
      <Flex w="100%" justifyContent="center" h="400px">
        <Box w="70%" mt="70px">
          <Carousel list={listProdutos} />
        </Box>
      </Flex>
      <Divider w="80%" my="20px"/>
      <Box
        h="400px"
        w="100%"
        display="flex"
        justifyContent="center"
        alignItems="center"
        mt="20px"
      >
        <Image
          src="/svg/logo.svg"
          alt="Picture of the author"
          width={600}
          height={600}
        />
      </Box>
      <Flex w="100%" justifyContent="center">
        <Box w="70%">
          <TransparencyArea />
        </Box>
      </Flex>
      {/* <Flex
        justifyContent="center"
        w="100%"
        h="400px"
        alignItems="center"
      >
        <Box w="70%" display="flex" justifyContent="center">
          <OurAction />
        </Box>
      </Flex> */}
    </Box>
  );
}
