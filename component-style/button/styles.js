 const ButtonStyles = {
  baseStyle: {},
  sizes: {
    md: {
      h: 10,
      fontFamily: 'heading',
      fontWeight: 'bold',
      fontSize: '14',
      px: 4,
    },
    lg: {
      h: 20,
      w: 300,
      fontFamily: 'heading',
      fontWeight: 'bold',
      fontSize: '20',
      px: 4,
    },
  },
  variants: {
    solid: {
      bg: 'primary.400',
      color: 'white',
      _active: {
        bg: 'primary.500',
      },
      _hover: {
        bg: 'primary.500',
        _disabled: {
          bg: 'primary.500',
        },
      },
      _disabled: {
        background: 'primary.400',
        opacity: '1',
      },
    },
    outline: {
      border: '1px solid',
      borderColor: 'primary.300',
      color: 'primary.300',
      bg: 'neutral.100',
      _hover: {
        bg: 'primary.100',
      },
      _active: {
        bg: 'primary.100',
      },
    },
    ghost: {
      bg: 'neutral.100',
      color: 'primary.300',
      _hover: {
        bg: 'primary.100',
      },
      _active: {
        bg: 'primary.100',
      },
    },
    'ghost-inverse': {
      bg: 'transparent',
      color: 'neutral.400',
      _hover: {
        bg: 'primary.100',
        color: 'primary.400',
      },
      _active: {
        bg: 'primary.100',
        color: 'primary.400',
      },
    },
    'ghost-transparent': {
      bg: 'transparent',
      color: 'neutral.400',
      border: 'none',
      _hover: {
        color: 'primary.400',
      },
      _active: {
        borderRadius: '0px',
        borderColor: 'secondary.400',
      },
    },
  },
  defaultProps: {
    variant: 'solid',
  },
}

export default ButtonStyles